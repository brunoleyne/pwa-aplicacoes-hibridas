const userKey = '_user'
const tokenKey = '_token'
const INITIAL_STATE = {
    forgotPassword : false,
    loading        : false,
    modal          : false,
    user           : isJson(localStorage.getItem(userKey)) ? JSON.parse(localStorage.getItem(userKey)) : null,
    token          : isJson(localStorage.getItem(tokenKey)) ? JSON.parse(localStorage.getItem(tokenKey)) : null
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'TOKEN_INVALIDATED':            
            localStorage.removeItem(userKey)
            localStorage.removeItem(tokenKey)

            return { ...state, user: null, token: null }
        case 'AUTHENTICATE':
            localStorage.setItem(tokenKey, JSON.stringify(action.payload))

            return { ...state, token: action.payload }
        case 'USER_LOGGED':
            const user = action.payload.data.item
            localStorage.setItem(userKey, JSON.stringify(user))

            return { ...state, user: user }
        case 'FORGOT_PASSWORD':
            return { ...state, forgotPassword: action.payload }
        case 'USER_LOADING':
            return { ...state, loading: action.payload }
        case 'PROFILE_MODAL':
            return { ...state, modal: action.payload }
        default:
            return state
    }
}

function isJson(str) {
    try { JSON.parse(str) } catch (e) { return false }

    return true
}