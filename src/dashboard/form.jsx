import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Field, reduxForm} from 'redux-form'

import {Col, Panel, Row, Table} from 'react-bootstrap'
import ContentHeader from '../common/template/contentHeader'
import Content from '../common/template/content'
import {Link} from 'react-router'
import {create, post, openClientModal, checkCPF, loadConversions} from './actions'
import LabelAndInput from '../common/form/labelAndInput'
import LabelAndTextarea from '../common/form/labelAndTextarea'
import ClientModal from './clientModal'


class Form extends Component {

    constructor(props) {
        super(props)
        this.state = {totalSale: 0,totalDonate: 0}
    }

    componentWillMount() {
        this.props.loadConversions()
        this.props.create()
    }

    convertMoneyToFloat(value) {
        if ( value.indexOf(",") !== -1) {
            value = value.replace(/[^0-9,.]/, '');
            if ( value.indexOf(",") !== -1) {
                value = value.replace(/\./g, '');
                value = value.replace(/,/g, '.');
            } else {
                value = value.replace(/,/, '.');
            }
        }
        return parseFloat(value)
    }

    calcConversionSale(conversionValue) {
       return parseInt(Math.round(this.state.totalSale/conversionValue))
    }

    calcConversionDonate(conversionValue) {
        return parseInt(Math.round(this.state.totalDonate/conversionValue))
    }

    componentWillReceiveProps(nextProps) {
        let totalSale = 0; let totalDonate = 0;
        if (nextProps !== undefined &&
            nextProps.fields !== undefined && 
            nextProps.fields.values !== undefined
        ) {
            const values = nextProps.fields.values

            if (values.sale_value) {
                totalSale += this.convertMoneyToFloat(values.sale_value)
            }

            if (values.donation_value) {
                totalDonate += this.convertMoneyToFloat(values.donation_value)
            }
        }

        this.setState({ totalSale : totalSale, totalDonate : totalDonate })
    }

    render() {
        const {handleSubmit} = this.props
        const required = value => (value ? undefined : 'Este campo é obrigatório')
        return (
            <div className='container-fluid'>
                <ClientModal/>
                <div className='row'>
                    <ContentHeader title='Vendas e Doações'>
                        <Link to='/' title='Voltar' className='button yellow br-bottom-right pull-right'>
                            <i className='fa fa-chevron-left' aria-hidden='true'></i> Voltar
                        </Link>
                    </ContentHeader>
                    <Content>
                            <div className="row">
                                <div className="form-group">
                                    <Field 
                                        name='cpf' 
                                        type='text' 
                                        component={LabelAndInput} 
                                        label='CPF*' 
                                        cols='6 6'
                                        tabIndex="1"
                                        className="br-bottom-right"
                                        validate={[required]}
                                        form='saleForm'
                                        onBlur={(event) => {
                                            if (this.props.fields !== undefined &&
                                                this.props.fields.values !== undefined
                                            ) {
                                                this.props.checkCPF(this.props.fields.values.cpf)
                                            }
                                        }}
                                        alt='cpf' />
                                    <div className="col-xs-6 col-sm-6">
                                        <input type="button" value="Cadastrar Cliente" tabIndex="-1" className="button green br-bottom-left"
                                               onClick={this.props.openClientModal} style={{marginTop: '27px'}}/>
                                    </div>

                                </div>
                            </div>
                        <form role='form' id='saleForm' onSubmit={handleSubmit(this.props.post)}>
                            <Row className="show-grid">
                                <Col sm={6} md={6}>
                                    <Panel defaultExpanded>
                                        <Panel.Heading>
                                            <Panel.Title toggle>
                                                Venda
                                            </Panel.Title>
                                        </Panel.Heading>
                                        <Panel.Collapse>
                                            <Panel.Body>
                                                <div className='row'>
                                                    <Field className="br-bottom-left" tabIndex="2" name='sale_value' type='text' component={LabelAndInput}
                                                           label='Valor*' cols='12 12' alt='decimal'/>
                                                </div>
                                            </Panel.Body>
                                        </Panel.Collapse>
                                    </Panel>
                                </Col>
                                <Col sm={6} md={6}>
                                    <Panel defaultExpanded>
                                        <Panel.Heading>
                                            <Panel.Title toggle>
                                                Doação
                                            </Panel.Title>
                                        </Panel.Heading>
                                        <Panel.Collapse>
                                            <Panel.Body>
                                                <div className='row'>
                                                    <Field className="br-bottom-left" tabIndex="3" name='donation_value' type='text' component={LabelAndInput}
                                                           label='Valor*' cols='12 12' alt='decimal'/>
                                                </div>
                                            </Panel.Body>
                                        </Panel.Collapse>
                                    </Panel>
                                </Col>
                            </Row>
                            <div className='row'>
                                <Field name='description' component={LabelAndTextarea}
                                       label='Descrição' cols='12 12' tabIndex="4" className="br-bottom-left"/>
                            </div>


                            <div className="row">
                                <div className="col-lg-12 form-group">
                                    <input type="submit" value="Salvar" tabIndex="5" className="button green br-top-left"/>
                                </div>
                            </div>
                        </form>

                        <Table striped bordered condensed hover responsive>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Ganho</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Cliente Venda</td>
                                    <td>{this.calcConversionSale(this.props.client_conversion)}</td>
                                </tr>
                                <tr>
                                    <td>Atendente Venda</td>
                                    <td>{this.calcConversionSale(this.props.attendant_conversion)}</td>
                                </tr>
                                <tr>
                                    <td>Gerente Venda</td>
                                    <td>{this.calcConversionSale(this.props.manager_conversion)}</td>
                                </tr>
                                <tr>
                                    <td>Cliente Doação</td>
                                    <td>{this.calcConversionDonate(this.props.client_donate_conversion)}</td>
                                </tr>
                                <tr>
                                    <td>Atendente Doação</td>
                                    <td>{this.calcConversionDonate(this.props.attendant_donate_conversion)}</td>
                                </tr>
                                <tr>
                                    <td>Gerente Doação</td>
                                    <td>{this.calcConversionDonate(this.props.manager_donate_conversion)}</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Content>
                </div>
            </div>
        )
    }
}

Form = reduxForm({form: 'salesForm', onSubmit: post})(Form)
const mapStateToProps = state => ({
    modal: state.sales.modal, fields: state.form.salesForm,
    manager_conversion: state.sales.manager_conversion, attendant_conversion: state.sales.attendant_conversion,
    client_conversion: state.sales.client_conversion, description: state.sales.description,
    manager_donate_conversion: state.sales.manager_donate_conversion, attendant_donate_conversion: state.sales.attendant_donate_conversion,
    client_donate_conversion: state.sales.client_donate_conversion,
})
const mapDispatchToProps = dispatch => bindActionCreators({post, create, openClientModal, checkCPF, loadConversions}, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Form)
