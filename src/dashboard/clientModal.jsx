import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import Modal from 'react-bootstrap/lib/Modal'
import {closeClientModal, openClientModal, submitModal} from "./actions";
import {Field, reduxForm} from 'redux-form'
import LabelAndInput from "../common/form/labelAndInput";

class ClientModal extends Component {
    render() {
        const required = value => (value ? undefined : 'Este campo é obrigatório')
        const {handleSubmit} = this.props
        return (
            <Modal
                show={this.props.modal}
                onHide={this.props.closeClientModal}
                dialogClassName="custom-modal"
            >
                <form role='form' onSubmit={handleSubmit(this.props.submitModal)}>
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-lg">
                            Cadastrar Cliente
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className='row'>
                            <Field name='cpf' type='text' component={LabelAndInput} label='CPF*' cols='12 12' alt='cpf'
                                   validate={[required]}/>
                        </div>
                        <div className='row'>
                            <Field name='name' type='text' component={LabelAndInput} label='Nome' cols='12 12' autoFocus/>
                        </div>
                        <div className='row'>
                            <Field name='email' component={LabelAndInput} type='email' label='E-mail' cols='12 12'/>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <input className="button purple br-bottom-right" type="button" value="Fechar" onClick={this.props.closeClientModal} />
                        <input className="button green br-bottom-left" type="submit" value="Salvar" />
                    </Modal.Footer>
                </form>
            </Modal>
        )
    }
}

ClientModal = reduxForm({form: 'clientModalForm', onSubmit: submitModal, enableReinitialize: true})(ClientModal)
const mapStateToProps = state => ({
    modal: state.sales.modal,
    initialValues: {
        cpf: state.form.salesForm.values ? state.form.salesForm.values.cpf : '',
    }
})
const mapDispatchToProps = dispatch => bindActionCreators({openClientModal, closeClientModal, submitModal}, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(ClientModal)
