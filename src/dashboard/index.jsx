import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import ContentHeader from '../common/template/contentHeader'
import Content from '../common/template/content'

class Dashboard extends Component {

    render() {
        const user = this.props.user || {}
        if (user.role_id !== 1) {
            return null
        }

        return (
            <div className='container-fluid'>
                <div className='row'>
                    <ContentHeader title='Dashboard'/>
                    <Content>

                    </Content>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.auth.user
})
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)