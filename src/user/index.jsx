import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import ContentHeader from '../common/template/contentHeader'
import Content from '../common/template/content'
import Modal from '../common/form/modal'
import List from './list'

import { Link } from 'react-router'
import { close, remove } from './actions'


class User extends Component {

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <ContentHeader title="Usuários">
                        <Link className="button green br-bottom-left pull-right" to="user/new" title="Cadastrar">
                            <i className="fa fa-plus"></i> Cadastrar
                        </Link>
                    </ContentHeader>
                    <Content> 
                        <List />
                    </Content>
                    <Modal {...this.props} />
                </div>
            </div>
        )
    }

}

const mapStateToProps = state => ({ 
    id      : state.user.id,
    modal   : state.user.modal,
    message : state.user.message,
    label   : state.user.label
})
const mapDispatchToProps = dispatch => bindActionCreators({ close, remove }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(User)
