import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { reduxForm, Field, formValueSelector, reset, initialize } from 'redux-form'

import ContentHeader from '../common/template/contentHeader'
import Content from '../common/template/content'
import { Link } from 'react-router'
import { post, update, create, edit } from './actions'
import LabelAndInput from '../common/form/labelAndInput'
import LabelAndSelect from '../common/form/labelAndSelect'


class Form extends Component {

    componentWillMount() {
        const id = this.props.params.id || false
        id ? this.props.edit(id) : this.props.create()
    }

    render() {
        const { handleSubmit } = this.props
        const submit = this.props.params.id ? this.props.update : this.props.post
        const readOnly = (this.props.params.id && !this.props.params.edit) ? true : false
        const required = value => (value ? undefined : 'Este campo é obrigatório')
        const validate = this.props.params.id ? [] : [ required ]

        let roles = [
            { id: '1', value: 'Administrador' },
            { id: '2' , value: 'Colaborador' }
        ]

        return (
            <div className='container-fluid'>
                <div className='row'>
                    <ContentHeader title='Usuário'>
                        <Link to='user' title='Voltar' className='btn btn-default pull-right'>
                            <i className='fa fa-chevron-left' aria-hidden='true'></i> Voltar
                        </Link>
                    </ContentHeader>
                    <Content>
                        <form role='form' onSubmit={handleSubmit(submit)} >
                            <div className='row'>
                                <Field name='name' type='text' component={LabelAndInput} label='Nome*' cols='12 6' validate={[required]} className='' readOnly={readOnly} />
                                <Field name='email' component={LabelAndInput} type='email' label='E-mail*' cols='12 6' validate={[required]} className='' readOnly={readOnly} />
                            </div>
                            <div className='row'>
                                <Field name='cpf' type='text' component={LabelAndInput} label='CPF*' cols='12 6' alt='cpf' validate={[required]} readOnly={readOnly} />
                                <Field name='role_id' component={LabelAndSelect} options={roles} label='Tipo de Usuário*' cols='12 6' validate={[required]} combobox={false} />
                            </div>
                            { this.props.params.id ? '' :
                            <div className='row'>
                                <Field name='password' component={LabelAndInput} type='password' label='Senha' cols='12 6' className='' validate={validate} />
                                <Field name='password_confirmation' component={LabelAndInput} type='password' label='Confirmação de Senha' cols='12 6' className='' validate={validate} />
                            </div> }
                            { readOnly ? '' :
                            <div className="row">
                                <div className="col-lg-12 form-group">
                                    <input type="submit" value="Salvar" className="btn btn-flat btn-primary" />
                                </div>
                            </div> }
                        </form>
                    </Content>
                </div>
            </div>
        )
    }
}

Form = reduxForm({form: 'userForm', onSubmit: post, destroyOnUnmount: false})(Form)
const selector = formValueSelector('userForm')
const mapStateToProps = state => ({
    user     : state.auth.user
})
const mapDispatchToProps = dispatch => bindActionCreators({ post, update, create, edit }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Form)