import axios from 'axios'
import { toastr } from 'react-redux-toastr'
import { initialize, getFormValues } from 'redux-form'
import { hashHistory } from 'react-router'
import { logout } from '../auth/authActions'
import {reset as resetForm} from 'redux-form'

import consts from '../consts'

export function index(page, sizePerPage, filters = []) {
    return dispatch => {
        axios.post(`${consts.API_URL}/users`, { page, sizePerPage, filters })
            .then(resp => { 
                dispatch({ type: 'USER_FETCHED', payload: resp.data })
            })
            .catch(e => { 

                if (typeof(e.response.data.message) == 'object') {
                    e.response.data.message.forEach(
                        error => toastr.error('Erro', error)
                    )
                } else {
                    toastr.error('Erro', e.response.data.message)
                }
            })
    }
}

export function create() {
    return dispatch => {
        dispatch(initialize('userForm', { }))
        }
}

export function edit(id) {
    return dispatch => {
        axios.get(`${consts.API_URL}/users/${id}`)
            .then(resp => {
                dispatch(initialize('userForm', resp.data.data))
            })
            .catch(e => {
                if (e.response.status == 401) {
                    dispatch(logout())
                    hashHistory.push('/')
                } else if (e.response.data.errors) {
                    Object.keys(e.response.data.errors).map(i => {
                        let errors = e.response.data.errors[i] || []
                        errors.forEach(error => toastr.error('Erro', error))
                    })
                } else {
                    toastr.error('Erro', e.response.data.message)
                }
            })
        }
}

export function post(values) {
    return submit(values, 'post')
}

export function update(values) {
    return submit(values, 'put')
}

function submit(values, method) {
    const id = values.id ? '/' + values.id : ''

    return dispatch => {
        axios[method](`${consts.API_URL}/usuarios${id}`, values)
            .then(resp => {
                toastr.success('Sucesso', 'Operação Realizada com sucesso.')
                hashHistory.push('/user')
            })
            .catch(e => {
                if (e.response.status == 401) {
                    dispatch(logout())
                    hashHistory.push('/')
                } else if (e.response.data.errors) {
                    Object.keys(e.response.data.errors).map(i => {
                        let errors = e.response.data.errors[i] || []
                        errors.forEach(error => toastr.error('Erro', error))
                    })
                } else {
                    toastr.error('Erro', e.response.data.message)
                }
            })
    }
}

export function modal(id) {
    return {
        type    : 'USER_MODAL_OPENED',
        payload : { id: id, message: 'Você deseja realmente inativar este usuário?', label: 'Inativar' },
    }
}

export function close() {
    return { type: 'USER_MODAL_CLOSED' }
}

export function resetOpenModal(id) {
    return {
        type: 'USER_RESET_MODAL_OPENED',
        payload: { id: id, message: 'Você deseja realmente enviar um e-mail para redefinição de senha para este usuário?' }
    }
}

export function reset(id) {
    return dispatch => {
        dispatch({ type: 'USER_LOADING', payload: true })
        axios.post(`${consts.API_URL}/user/reset/${id}`)
            .then(resp => {
                dispatch(close())
                dispatch({ type: 'USER_LOADING', payload: false })
                toastr.success('Sucesso', 'Enviamos uma nova senha para o e-mail deste usuário.')
            })
            .catch(e => {
                dispatch({ type: 'USER_LOADING', payload: false })
                if (e.response.data.errors) {
                    Object.keys(e.response.data.errors).map(i => {
                        let errors = e.response.data.errors[i] || []                        
                        errors.forEach(error => toastr.error('Erro', error))
                    })
                } else {
                    toastr.error('Erro', e.response.data.message)
                }
            })
    }
}