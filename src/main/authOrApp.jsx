import '../common/template/dependencies'
import React, { Component } from 'react'
import axios from 'axios'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import App from './app'
import Auth from '../auth/auth'
import { authenticate, logout } from '../auth/authActions'


class AuthOrApp extends Component {   

    render() {        
        const { token } = this.props.auth
        
        if (token) {
            axios.defaults.headers.common['Authorization'] = `${token.token_type} ${token.access_token}`

            return <App>{this.props.children}</App>
        } else {
            return <Auth {...this.props} />
        }
    }

}

const mapStateToProps = state => ({ auth: state.auth })
const mapDispatchToProps = dispatch => bindActionCreators({ authenticate, logout }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(AuthOrApp)