import React from 'react'
import Modal from 'react-bootstrap/lib/Modal'
import Button from 'react-bootstrap/lib/Button'


export default props => {
    const { modal, message, id } = props
    const color = props.color || 'danger'
    const label = props.label || 'Deletar'

    return (
        <Modal show={modal} onHide={props.close}>
            <Modal.Header closeButton>
                <Modal.Title>Confirmação</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>{ message }</p>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.close}>Cancelar</Button>
                <Button bsStyle={color} onClick={() => props.remove(id)}>{label}</Button>
            </Modal.Footer>
        </Modal>
    )
}