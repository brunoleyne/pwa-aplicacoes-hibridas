import React, { Component } from 'react'
import Grid from "../layout/grid";


export default class LabelAndInput extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        const props = this.props
        const className = props.className || ''

        return(
            <div className='form-group'>
                <input {...props.input}
                       className={`${className} form-control ${props.alt}`}
                       autoFocus={props.autoFocus}
                       tabIndex={props.tabIndex ? props.tabIndex : ''}
                       placeholder={props.placeholder}
                       type={props.type} />
                {props.meta.touched && ((props.meta.error && <span className='info'>{props.meta.error}</span>) || (props.meta.warning && <span className='info'>{props.meta.warning}</span>))}
            </div>
        )
    }

}