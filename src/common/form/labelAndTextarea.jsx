import React from 'react'
import Grid from '../layout/grid'

export default props => {
    const style = props.style || {}
    const readOnly = props.readOnly || false
    const className = props.className || ''
    const rows = props.rows || '1'
    const tabIndex = props.tabIndex || false

    return(
        <Grid cols={props.cols} style={style}>
            <div className='form-group'>
                <label htmlFor={props.name}>{props.label}</label>
                {
                    readOnly ? <span style={{paddingLeft: '10px'}}>{ props.input.value }</span> :
                    <textarea rows={rows} {...props.input} className={`${className} form-control`} tabIndex={tabIndex}>
                        { props.input.value }
                    </textarea>
                }
                {props.meta.touched && ((props.meta.error && <span className='info'>{props.meta.error}</span>) || (props.meta.warning && <span className='info'>{props.meta.warning}</span>))}
            </div>
        </Grid>
    )
}