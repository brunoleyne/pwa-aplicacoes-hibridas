import React from 'react'
import Grid from '../layout/grid'
import Select, { Option } from 'rc-select'
import 'rc-select/assets/index.css'

export default props => {
    const empty = () => {}
    const style = props.style || {}
    const readOnly = props.readOnly || false
    const handleSelect = props.handleSelect || empty
    let value = props.input.value ? props.input.value : []

    if (readOnly) value = value.join(', ')

    return (
        <Grid cols={props.cols} style={style}>
            <div className='form-group'>
                <label htmlFor={props.name}>{props.label}</label>
                { readOnly ? <span style={{paddingLeft: '10px'}}>{value}</span> :
                <Select
                    multiple
                    allowClear
                    showSearch={false}
                    animation={'slide-up'}
                    choiceTransitionName="rc-select-selection__choice-zoom"
                    tokenSeparators={[' ', ',']}
                    placeholder="Selecione..."
                    dropdownMenuStyle={{overflow: 'auto'}}
                    dropdownStyle={{ maxHeight: 200, overflow: 'auto', zIndex: 1500 }}
                    className="form-control"
                    style={{border: 'none', padding: '0'}}
                    value={value}
                    onChange={props.input.onChange}
                    onBlur={event => { props.input.onChange(event) }}
                    onSelect={value => {
                        props.input.onChange(value)
                        handleSelect(value)
                    }} >
                    { 
                        Object.keys(props.options).map((value, index) => {
                            let item = props.options[value]
                            return (
                                <Option key={index} value={`${item.value}`} title={item.value}>{item.value}</Option>
                            )
                        }) 
                    }
                </Select> }
                {props.meta.touched && ((props.meta.error && <span className='info'>{props.meta.error}</span>) || (props.meta.warning && <span className='info'>{props.meta.warning}</span>))}
            </div>
        </Grid>
    )
}