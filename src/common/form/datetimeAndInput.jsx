import React from 'react'
import Grid from '../layout/grid'

import DatePicker from 'react-bootstrap-date-picker'

export default props => {
    const dayLabels = ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"]
    const monthLabels = ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"]

    return (
        <Grid cols={props.cols}>
            <div className='form-group'>
                <label htmlFor={props.name}>{props.label}</label>
                <DatePicker 
                    {...props.input} 
                    dateFormat='DD/MM/YYYY'
                    dayLabels={dayLabels} 
                    monthLabels={monthLabels} 
                    placeholder={props.placeholder}
                />
                {props.meta.touched && ((props.meta.error && <span className='info'>{props.meta.error}</span>) || (props.meta.warning && <span className='info'>{props.meta.warning}</span>))}
            </div>
        </Grid>
    )
}