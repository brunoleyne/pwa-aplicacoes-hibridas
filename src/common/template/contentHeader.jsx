import React from 'react'

export default props => (
    <div className="content-header">
		<h1 className="with-button">
            { props.title }
            { props.children }
        </h1>        
	</div>
)