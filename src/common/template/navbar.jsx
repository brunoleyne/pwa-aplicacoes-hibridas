import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { logout } from '../../auth/authActions'


class Navbar extends Component {
    constructor(props) {
        super(props)
        this.state = { open: false }
    }

    render() {
        const user = this.props.user || {}
        return (
            <div className='navbar-custom-menu'>
                <ul className='nav navbar-nav'>
                    <a className='button' href='#' onClick={this.props.logout}>Sair</a>
                </ul>
            </div>
        )
    }
}

const mapStateToProps = state => ({user: state.auth.user})
const mapDispatchToProps = dispatch => bindActionCreators({ logout }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Navbar)
