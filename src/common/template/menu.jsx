import React, {Component} from 'react'
import {connect} from 'react-redux'

import MenuItem from './menuItem'


class Menu extends Component {

    componentDidMount() {
        $(document).ready(function () {
            $('.main-sidebar ul').tree()
        })
    }

    render() {
        return (
            <ul className='sidebar-menu tree' data-widget='tree'>
                <MenuItem path='user' label='Usuários' icon='user-o'/>
                <MenuItem path='ordem-servico' label='Ordem Serviço' icon='circle-o'/>
            </ul>
        )
    }
}

const mapStateToProps = state => ({user: state.auth.user})
export default connect(mapStateToProps, null)(Menu)