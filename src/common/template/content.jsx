import React from 'react'

export default props => (
    <div className='content'>
        <div className="row">
		    <div className="col-xs-12">
			    <div className="box box-primary content-box">
				    <div className="box-body">
                        { props.children }
                    </div>
                </div>
            </div>
        </div>
    </div>
)